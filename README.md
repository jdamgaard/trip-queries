What is this
============

For this exercise, we’d like you to implement a system that mimics some of the types of data we see here at Uber.  There are at least two components:

1. The first component will continuously generate trip data, issuing messages for the following events:
    - Trip Begin (data: trip id, latitude, longitude)
    - Roughly once per second, Trip Ping (data: trip id, latitude, longitude)
    - Trip End (data: trip id, latitude, longitude)

2. The second component will consume these events, store them in memory or on disk, and expose an interface for querying the data.  Types of queries we’d like to be able to answer are:
    - Dollar fare for a trip (assuming all trips occurs in e.g. Atlanta)
    - How many trips passed through a given geo-rect (defined by latitude/longitude pair)
    - How many trips started or stopped within a given geo-rect, and the sum of their fares
    - How many trips were occurring at a given point in time
3. Some bounding conditions:
    - Assume a system with up to 500 concurrent trips at any given time
    - Assume further that we require interactive query times
 
You can use whichever languages and libraries you like, but preferably not Java.  The components can talk to each other how you see fit, but should use a network protocol. Please note that the project should be "as if it were going into production".  You can host the code on  AWS, GCE/GAE, Heroku, Nodejitsu, etc.  When done please send github/bitbucket link and hosted app link.  
 
Bonus points for a system architecture that can potentially scale to much higher loads than above, and/or for a Web UI that visualizes the system activity.

Description
===========

## Technologies used

- Node.js
    - express
- MySQL 5.7.7 (spatial functions are used) 
- Docker

## Notes
### MySQL
I has been a couple of years since I last used MySQL, but the new spatial functions seems interesting.
I do not know how well these spatial functions scale, in my small scale tests they worked well,
 and I assume they can scale to the required load, even when the system runs for several days and much more data is queried.
I have not tuned the MySQL config to take advantage of the server hardware, so query times could easily be improved by additional tuning.

### Docker
This is my first project using docker, but it seems nice to handle the environment for the app.
I have split the app into 3 containers

  * Trip generator
  * Server
  * Database
     
A docker compose file could probably make deployment even easier, but I did not try that.
Normally I use forever to make sure node.js apps keeps running, in this project I have not used any monitoring/supervisor which is something to improve! 

### Polishing
A frontend could be nice, but was not prioritized for this project.
Also automated testing of remaining parts of code would be nice to have.
A function to stop the generator, where it ends its active trips, would make it possible to upgrade the generator without "dangling" started trips that never ends. 

### Security
At the moment security has not been prioritized, as the system is assumed to be deployed on a secure network.
An auth middleware could be added.


### Architecture
The current architecture handles 1000+ concurrent trips.
If trips are much longer than simulated, or many packets are delayed, the cpu usage could increase, creating a bottleneck.
Also when much data is stored in the db, the operations and overhead for keeping indexes updated might start to take more time, which could potentially be a bottleneck.
The database could however be distributed, such that each table is on a different server.
The server app could also be replicated, if a load balancer is introduced, then the trips could be distributed e.g. based on ids.
If the database is also sharded according to the trip id, then the updates could scale to much higher loads.
The queries could still be performed by aggregation of the results from each DB.   

Profiling might highlight points in the code that could be optimized to increase throughput of a node even more.

I have made the components, such that the system can recover if the server component is shut down temporarily, or if the db is rebooted, but a new failure during recovery, is not tested.
As the recovery of the sever component involves expensive queries to the db, it can take som time to complete.

How to Run
----------

## Build Docker
The docker images are available through docker hub, so this build process should not be needed for running the project.

    * .../producer $ docker build -t damgaard/uber-generator
    * .../server $ docker build -t damgaard/uber-server


## Setup
The only manual step during the setup, is database creation based on the table.sql file from this repository.
 
Remove port forwarding of db, for more security.

    * docker run --name uber-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=pw -d mysql:5.7.7
    * docker exec uber-mysql mysql --password=pw -e "CREATE DATABASE uber_traffic;"
    * docker exec uber-mysql mysql --password=pw -e "CREATE DATABASE uber_test;"
    * docker exec -i uber-mysql mysql --password=pw uber_traffic < tables.sql
    * docker exec -i uber-mysql mysql --password=pw uber_test < tables.sql
    * docker run --name uber-server -e db:host=database -e db:pass=pw --link uber-mysql:database -p 80:8088 -d damgaard/uber-server
    * docker run --name uber-generator -e SERVER=serv  --link uber-server:serv -d damgaard/uber-generator

## Start / stop
After the images are create on the local machine, after a reboot restart with the following commands

    * docker start uber-mysql
    * docker start uber-server
    * docker start uber-generator

    * docker stop uber-mysql
    * docker stop uber-server
    * docker stop uber-generator

## Run tests
Perform unit and integration tests

    * docker exec uber-server gulp test

## See stats for load while runnning
The apps prints load stats to STDOUT at regular intervals, see them using

    * docker logs -f uber-generator
    * docker logs -f uber-server

## Reset db
    * docker exec -i uber-mysql mysql --password=pw uber_traffic < tables.sql

## Remove docker containers
    * docker rm uber-mysql
    * docker rm uber-server
    * docker rm uber-generator

How to use
----------
A demostration is running at the server
46.4.112.181 port 80

    * Get fare for a specific trip
    /fare?id=<id>
    eg. /fare?id=100
    http://46.4.112.181/fare?id=100

    * Get number of completed trips intersecting with the specified geo-rect
    /tripIntersects?rect=<lat1>,<lon1>,<lat2>,<lon2>
    http://46.4.112.181/tripIntersects?rect=56.0009,10.1000,56.1000,10.1001
    
    * Get number of trips and sum of fares for all completed trips starting and/or ending inside the specified geo-rect
    The type parameter can be used to specify whether to include trips beginning or ending, only trips beginning, or only trips ending inside the specified geo-rect
    /tripLocation?rect=<lat1>,<lon1>,<lat2>,<lon2>&type=<all|begin|end|>
    http://46.4.112.181/tripLocation?rect=56.1,10.1,56.2,10.15&type=all

    * Get #concurrent trips at a given timestamp (in ms)  
    /concurrentTrips?ts=<ts>
    http://46.4.112.181/concurrentTrips?ts=1434530000000

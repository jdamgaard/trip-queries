DROP TABLE IF EXISTS `rawPoints`;
CREATE TABLE `rawPoints` (
  `id` int(11) NOT NULL,
  `ts` bigint(20) NOT NULL,
  `lat` float(10,7) NOT NULL,
  `lon` float(10,7) NOT NULL,
  PRIMARY KEY (`ts`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `concurrentTrips`;
CREATE TABLE `concurrentTrips` (
  `ts` bigint(20) NOT NULL,
  `trips` int(11) NOT NULL,
  PRIMARY KEY (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `trips`;
CREATE TABLE `trips` (
  `id` int(11) NOT NULL,
  `startLat` float(10,7) NOT NULL,
  `startLon` float(10,7) NOT NULL,
  `endLat` float(10,7) DEFAULT NULL,
  `endLon` float(10,7) DEFAULT NULL,
  `fare` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `trajectories`;
CREATE TABLE `trajectories` (
  `id` int(11) NOT NULL,
  `trajectory` LINESTRING NOT NULL,
  PRIMARY KEY (`id`),
  SPATIAL INDEX(`trajectory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

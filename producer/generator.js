/**
 * Created by jacob on 6/12/15.
 */
'use strict';

var http = require('http'),
    nconf = require('nconf');

nconf.argv()
    .env()
    .file({ file: './config.json' });

var serverUrl = nconf.get('SERVER');
var serverPort = nconf.get('PORT');

var concurrent = nconf.get('MAX_TRIPS');
var maxWorkers = 200;

//Area where rides start
var AarhusMinLat = 56.080484;
var AarhusMaxLat = 56.260330;
var AarhusMinLong = 10.093830;
var AarhusMaxLong = 10.238516;

var running = 0;

var sendQueue = [];
var sendWorkers = 0;
var lastError;

function chooseRandomStartAarhus() {
    var lat = (AarhusMinLat + Math.random() * (AarhusMaxLat - AarhusMinLat));
    var lon = AarhusMinLong + Math.random() * (AarhusMaxLong - AarhusMinLong);
    return [lat, lon];
}

/**
 * Request id for next trip from server
 */
function getId(cb) {
    var options = {
        hostname: serverUrl,
        port: serverPort,
        path: '/getNextId',
        method: 'GET'
    };

    var req = http.request(options,
        function (res) {
            if (res.statusCode === 200 || res.statusCode === 202) {
                var data = '';
                res.on('data', function (chunk) {
                    data += chunk;
                });
                res.on('end', function () {
                    var result = JSON.parse(data);
                    cb(null, result.id);
                });
            }
            if (res.statusCode >= 400) {
                var str = '';
                res.on('data', function (chunk) {
                    str += chunk;
                });
                res.on('end', function () {
                    cb(new Error(res.statusCode + ' - ' + str));
                });
            }
        }
    );

    req.setTimeout(5000, function() {
        cb(new Error('Timed out'));
    });

    req.on('error', function(e) {
        cb(e);
    });

    req.end();
}

function sendToServer(event, contents, cb) {
    sendWorkers++;

    var options = {
        hostname: serverUrl,
        port: serverPort,
        path: '/' + event,
        method: 'POST',
        agent: false, //since connection pooling results in "EADDRNOTAVAIL" on high load 1000+ requests/s
        //http://stackoverflow.com/questions/21859537/connect-eaddrnotavail-in-nodejs-under-high-load-how-to-faster-free-or-reuse-tc
        //keepAlive: false,
        headers: {
            'content-type': 'application/json',
            'content-length': contents.length
        }
    };

    var req = http.request(options);

    req.setTimeout(5000, function() {
        cb('Timed out');
        req.abort();
        sendQueue.push([event, contents]);
    });

    req.on('error', function(e) {
        sendQueue.push([event, contents]);
        cb(e);
    });

    req.on('response', function(res) {
        if (res.statusCode === 200 || res.statusCode === 202) {
            res.resume();
            cb();
        } else if (res.statusCode >= 400) {
            // server did not like the data!
            // do not resend
            var str = '';
            res.on('data', function (chunk) {
                str += chunk;
            });
            res.on('end', function () {
                cb(res.statusCode + ' - ' + str);
            });
        }
    });

    req.on('close', function() {
        sendWorkers--;
    });

    req.write(contents);
    req.end();
}

function sendFromQueue() {
    if (sendQueue.length === 0) {
        return;
    }

    var e = sendQueue.shift();
    var event = e[0];
    var contents = e[1];

    sendToServer(event, contents, function(err) {
        if (err) {
            lastError = err;
            return;
        }

        sendFromQueue();
        //if long queue increase number of workers
        if (sendQueue.length > sendWorkers && sendWorkers < maxWorkers) {
            sendFromQueue();
        }
    });
}

function logPoint(id, ts, point, event) {
    if (event === undefined) {event = 'ping';}
    var contents = '{"id":'+id+', "ts":'+ts+', "lat":'+ point[0].toFixed(7)+', "lon":'+point[1].toFixed(7)+'}';
    sendQueue.push([event, contents]);
    if (sendWorkers < 10) {
        sendFromQueue();
    }
}

var nextPoint = function (id, lastPoint) {
    //random walk, speed is capped at ~ 140km/t
    var s1 = Math.random() * 0.00050 - 0.00025;
    var s2 = Math.random() * 0.00090 - 0.00045;
    var newPoint = [lastPoint[0] + s1, lastPoint[1] + s2];

    //stop at random
    if (Math.random() < 0.0015) {
        logPoint(id, Date.now(), newPoint, 'end');
        running--;
        return;
    }
    //else continue to next point
    logPoint(id, Date.now(), newPoint);
    setTimeout(nextPoint, 1000, id, newPoint);
};


//if less than max concurrent, start a new trip with 50% probability.
function startTrip() {
    if (running < concurrent && Math.random() < 0.5) {
        running++;
        getId(function(err, id) {
            if (err) {
                running--;
                if (err.code === 'ECONNREFUSED') {
                    return console.log('Server is Down!');
                } else {
                    return console.log(err);
                }
            }
            //only start trip, if id is returned from server.
            var firstPoint = chooseRandomStartAarhus();
            // The begin-event should be accepted, before the trip is started
            var contents = '{"id":'+id+', "ts":'+Date.now()+', "lat":'+ firstPoint[0].toFixed(7)+', "lon":'+firstPoint[1].toFixed(7)+'}';
            sendToServer('begin', contents, function (err) {
                if (err) {
                    running--;
                   console.log(err);
                } else {
                    //no errors, start trip
                    setTimeout(nextPoint, 1000, id, firstPoint);
                }
            });
        });
    }
}

//start
setInterval(startTrip, nconf.get('NEW_TRIP_FREQ') * 1);

//print stats for last interval
function logStats() {
    if (lastError) {
        console.log('Last error: ' + lastError);
        lastError = null;
    }

    console.log('Active trips:' + running + '\tQueued msg:' + sendQueue.length + '\tWorkers:' + sendWorkers);
}
setInterval(logStats, nconf.get('LOG_FREQ') * 1);

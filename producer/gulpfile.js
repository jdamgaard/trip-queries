'use strict';

var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    gJasmine = require('gulp-jasmine'),
    runSequence = require('run-sequence');


gulp.task('test:unit', function () {
    return gulp.src('./*.spec.js')
        .pipe(gJasmine({
            //verbose: true,
            includeStackTrace: true
        }));
    //.on('error', function(err) {console.log(err)});
});

gulp.task('test:integration', function () {
    return gulp.src('./*.integration.js')
        .pipe(gJasmine({
            //verbose: true,
            includeStackTrace: true
        }));
    //.on('error', function(err) {console.log(err)});
});

gulp.task('test:all', function(callback) {
    runSequence(
        'test:unit',
        'test:integration',
        //'other test',
        function(err) {// jshint ignore:line
            //if (err) {console.log(err);}
            callback();
        });
});

gulp.task('lint', function () {
    return gulp.src('./*.js')
        .pipe(jshint('./.jshintrc'))
        //.pipe(customErrorReporter);
        .pipe(jshint.reporter('default'));

});

gulp.task('watch', ['lint'], function () {
    gulp.watch('./*.js', ['test:unit', 'test:integration', 'lint']);
});

gulp.task('test', ['lint','test:unit', 'test:integration'], function () {
    console.log('done');
});
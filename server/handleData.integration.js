'use strict';

var rewire = require('rewire');

var mysql = require('mysql'),
    async = require('async'),
    nconf = require('nconf');

nconf.argv()
    .env()
    .file({file: './config.json'});

var connection = mysql.createConnection({
    host: nconf.get('db:host'),
    user: nconf.get('db:user'),
    password: nconf.get('db:pass'),
    database: nconf.get('db:testDB')
});

var pool = mysql.createPool({
    connectionLimit: nconf.get('db:connectionLimit'),
    host: nconf.get('db:host'),
    user: nconf.get('db:user'),
    password: nconf.get('db:pass'),
    database: nconf.get('db:testDB')
});

//rewire handleData to use test db
var handleData = rewire('./handleData');
handleData.__set__('connection', connection);
handleData.__set__('pool', pool);


function resetDB(cb) {
    async.parallel(
        [
            function (done) {
                pool.query('truncate `concurrentTrips`', function (err) {
                    done(err);
                });
            },
            function (done) {
                pool.query('truncate `trips`', function (err) {
                    done(err);
                });
            },
            function (done) {
                pool.query('truncate `trajectories`', function (err) {
                    done(err);
                });
            }
        ],
        cb
    );
}

describe('MySQL update queries:', function () {
    beforeEach(function (done) {
        resetDB(function (err) {
            if (err) {
                console.log('Error: ' + err.message);
            } else {
                done();
            }
        });
    });

    describe('Concurrent trips:', function () {

        it('should insert on empty db', function (done) {
            handleData.beginTrip(1, 1, 1, 1, function (err) {
                expect(err).toBeFalsy();
                done();
            });
        });

        it('should count 0 on empty table', function (done) {
            handleData.getConcurrentTrips(1000, function (err, res) {
                expect(res).toBe(0);
                done();
            });
        });

        it('should count 3 trips correctly', function (done) {
            async.parallel(
                [
                    function (cb) {
                        handleData.beginTrip(1, 1, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.beginTrip(2, 2, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.beginTrip(3, 3, 1, 1, cb);
                    }
                ],
                function (err) {
                    expect(err).toBe(null);
                    handleData.getConcurrentTrips(4, function (err, res) {
                        expect(res).toBe(3, '3 trips are active at time 4');
                        done();
                    });
                }
            );
        });

        it('should handle endEvent', function (done) {
            handleData.endTrip(1, 1, 1, 1, 10, done);
            //updateConcurrent(1,-1, done);
        });

        it('should count begin end', function (done) {
            //begin trips
            async.series(
                [
                    function (cb) {
                        handleData.beginTrip(1, 1, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.endTrip(1, 2, 1, 1, 10, cb);
                    }
                ],
                function (err) {
                    expect(err).toBe(null);
                    //end trips
                    async.parallel([
                            function (cb) {
                                handleData.getConcurrentTrips(1, function (err, res) {
                                    expect(res).toBe(1);
                                    cb();
                                });
                            },
                            function (cb) {
                                handleData.getConcurrentTrips(2, function (err, res) {
                                    expect(res).toBe(0);
                                    cb();
                                });
                            }
                        ],
                        function (err) {
                            done(err);
                        }
                    );
                }
            );
        });

        it('should simoultaneous trips correctly', function (done) {
            async.parallel(
                [
                    function (cb) {
                        handleData.beginTrip(1, 1, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.beginTrip(2, 2, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.beginTrip(3, 3, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.beginTrip(4, 2, 1, 1, cb);
                    }
                ],
                function (err) {
                    expect(err).toBe(null);
                    async.parallel([
                            function (cb) {
                                handleData.getConcurrentTrips(2, function (err, res) {
                                    expect(res).toBe(3, '3 trips are active at time 2');
                                    cb();
                                });
                            },
                            function (cb) {
                                handleData.getConcurrentTrips(3, function (err, res) {
                                    expect(res).toBe(4, '4 trips are active at time 3');
                                    cb();
                                });
                            }
                        ],
                        function (err) {
                            done(err);
                        }
                    );
                }
            );
        });

        it('should count multiple trips correctly', function (done) {
            //add trips to db
            async.parallel(
                [
                    function (cb) {
                        handleData.beginTrip(1, 1, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.beginTrip(2, 2, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.beginTrip(3, 3, 1, 1, cb);
                    },
                    function (cb) {
                        handleData.endTrip(1, 15, 1, 1, 15, cb);
                    },
                    function (cb) {
                        handleData.endTrip(2, 10, 1, 1, 15, cb);
                    },
                    function (cb) {
                        handleData.endTrip(3, 7, 1, 1, 15, cb);
                    }
                ],
                function (err) {
                    expect(err).toBe(null);
                    //verify queries returns correct number
                    async.parallel([
                            function (cb) {
                                handleData.getConcurrentTrips(4, function (err, res) {
                                    expect(res).toBe(3, '3 trips are active at time 4');
                                    cb();
                                });
                            },
                            function (cb) {
                                handleData.getConcurrentTrips(7, function (err, res) {
                                    expect(res).toBe(2);
                                    cb();
                                });
                            },
                            function (cb) {
                                handleData.getConcurrentTrips(18, function (err, res) {
                                    expect(res).toBe(0);
                                    cb();
                                });
                            },
                            function (cb) {
                                handleData.getConcurrentTrips(11, function (err, res) {
                                    expect(res).toBe(1);
                                    cb();
                                });
                            }
                        ],
                        function (err) {
                            done(err);
                        }
                    );
                }
            );
        });
    });

    describe('Update fare:', function () {

        it('should update fare if requested', function (done) {
            async.series([
                function (cb) {
                    //start trip
                    handleData.beginTrip(1, 1, 1, 1, cb);
                },
                function (cb) {
                    //set fare = 10
                    handleData.updateFare(1, 10, cb);
                },
                function (cb) {
                    //test fare is 10
                    handleData.getTripFare(1, function (err, res) {
                        expect(res).toBe(10);
                        cb();
                    });
                },
                function (cb) {
                    //end trip and set fare = 15
                    handleData.endTrip(1, 10, 5, 5, 15, cb);
                },
                function (cb) {
                    //test fare is 15
                    handleData.getTripFare(1, function (err, res) {
                        expect(res).toBe(15);
                        cb();
                    });
                },
                function (cb) {
                    //set fare = 10
                    handleData.updateFare(1, 10, cb);
                },
                function (cb) {
                    //test fare is 10
                    handleData.getTripFare(1, function (err, res) {
                        expect(res).toBe(10);
                        cb();
                    });
                }
            ], function (err) {
                expect(err).toBeFalsy();
                done();
            });
        });


        it('should not crash when update of non existing trip', function (done) {
            handleData.updateFare(200, 10, function (err) {
                expect(err).toBeFalsy();
                done();
            });
        });
    });

});

describe('MySQL queries:', function () {
    describe('Get fare from geo-rect:', function () {
        beforeAll(function (done) {
            //reset db, start trips, end trips
            async.series([
                    function (callback) {
                        resetDB(callback);
                    },
                    function (callback) {
                        async.parallel(
                            [
                                function (cb) {
                                    handleData.beginTrip(1, 1, 50, 10, cb);
                                },
                                function (cb) {
                                    handleData.beginTrip(2, 2, 50, 10.12, cb);
                                },
                                function (cb) {
                                    handleData.beginTrip(3, 3, 50, 10, cb);
                                },
                                function (cb) {
                                    handleData.beginTrip(4, 1, 50.15, 10.11, cb);
                                },
                                function (cb) {
                                    handleData.beginTrip(5, 2, 50.12, 10.15, cb);
                                }
                            ], function (err) {
                                callback(err);
                            });
                    },
                    function (callback) {
                        async.parallel(
                            [

                                function (cb) {
                                    handleData.endTrip(1, 10, 51.10, 11.00, 11, cb);
                                },
                                function (cb) {
                                    handleData.endTrip(2, 10, 50.15, 10.12, 14, cb);
                                },
                                function (cb) {
                                    handleData.endTrip(3, 10, 50.15, 10.05, 15, cb);
                                },
                                function (cb) {
                                    handleData.endTrip(4, 10, 50.00, 10.10, 16, cb);
                                },
                                function (cb) {
                                    handleData.endTrip(5, 10, 50.16, 10.13, 17, cb);
                                }], function (err) {
                                callback(err);
                            });
                    }
                ],
                function (err) {
                    expect(err).toBeFalsy();
                    done();
                }
            );
        });


        it('should select points starting in geo-rect', function (done) {
            handleData.getTripFareFromGeoRect(50.1, 10.1, 50.2, 10.2, 'begin', function (err, res) {
                expect(err).toBeFalsy();
                expect(res).toBeTruthy();
                expect(res.trips).toBe(2);
                expect(res.fare).toBe(16 + 17);
                done();
            });
        });

        it('should select points ending in geo-rect', function (done) {
            handleData.getTripFareFromGeoRect(50.1, 10.1, 50.2, 10.2, 'end', function (err, res) {
                expect(err).toBeFalsy();
                expect(res).toBeTruthy();
                expect(res.trips).toBe(2);
                expect(res.fare).toBe(14 + 17);
                done();
            });
        });

        it('should select points starting or ending in geo-rect', function (done) {
            handleData.getTripFareFromGeoRect(50.1, 10.1, 50.2, 10.2, 'all', function (err, res) {
                expect(err).toBeFalsy();
                expect(res).toBeTruthy();
                expect(res.trips).toBe(3);
                expect(res.fare).toBe(14 + 16 + 17);
                done();
            });
        });
    });

    describe('Get intersecting trips from geo-rect:', function () {

        beforeAll(function (done) {
            resetDB(function (err) {
                expect(err).toBeFalsy();
                //prepared for adding additional points in parallel
                async.parallel(
                    [
                        function (cb) {
                            var points = [
                                [1, 50, 10],
                                [1, 50, 10.1],
                                [1, 50, 10.2],
                                [1, 50.1, 10.3],
                                [1, 50.2, 10.4],
                                [1, 50.3, 10.5],
                                [1, 50.4, 10.5],
                                [1, 50.5, 10.5]
                            ];
                            handleData.saveTrajectory(1, points, cb);
                        }
                        //,
                        //function (cb) {
                        //    handleData.saveTrajectory(2, points, cb);
                        //}
                    ], function (err) {
                        expect(err).toBeFalsy();
                        done();
                    });
            });
        });

        it('should not count if no intersection', function (done) {
            handleData.getTripsIntersectingWithRect(5, 5, 6, 6, function (err, res) {
                expect(err).toBeFalsy();
                expect(res).toBe(0);
                done();
            });
        });

        it('should not count if no intersection even if inside MBR', function (done) {
            handleData.getTripsIntersectingWithRect(50.17, 10.25, 50.2, 10.35, function (err, res) {
                expect(err).toBeFalsy();
                expect(res).toBe(0);
                done();
            });
        });

        it('should count if point is contained in geo-rect', function (done) {
            handleData.getTripsIntersectingWithRect(50.15, 10.15, 50.45, 10.45, function (err, res) {
                expect(err).toBeFalsy();
                expect(res).toBe(1);
                done();
            });
        });

        it('should count if line segment intersection', function (done) {
            handleData.getTripsIntersectingWithRect(50.15, 10.25, 50.16, 10.45, function (err, res) {
                expect(err).toBeFalsy();
                expect(res).toBe(1);
                done();
            });
        });

    });

    describe('Save and update trajectory:', function () {
        beforeEach(function (done) {
            resetDB(function (err) {
                if (err) {
                    console.log('Error: ' + err.message);
                } else {
                    done();
                }
            });
        });

        it('should save trajectory', function (done) {
            var points = [
                [1, 50, 10],
                [1, 50, 10.1],
                [1, 50, 10.2],
                [1, 50.1, 10.3],
                [1, 50.2, 10.4],
                [1, 50.3, 10.5],
                [1, 50.4, 10.5],
                [1, 50.5, 10.5]
            ];
            handleData.saveTrajectory(1, points, function(err) {
                expect(err).toBeFalsy();
                pool.query('SELECT id, ST_AsText(trajectory) trajectory from trajectories WHERE id=1', function (err, result) {
                    expect(result[0].id).toBe(1);
                    var p = points.map(function(e) {return e[1] + ' ' + e[2];}).join(',');
                    expect(result[0].trajectory).toBe('LINESTRING('+p+')');
                    done();
                });
            });
        });

        it('should update trajectory', function (done) {
            var points = [
                [1, 50, 10],
                [1, 50, 10.1],
                [1, 50, 10.2],
                [1, 50.1, 10.3],
                [1, 50.2, 10.4],
                [1, 50.3, 10.5],
                [1, 50.4, 10.5],
                [1, 50.5, 10.5]
            ];
            handleData.saveTrajectory(1, points, function(err) {
                expect(err).toBeFalsy();
                points[1][2] = 49.9;
                handleData.updateTrajectory(1, points, function(err) {
                    expect(err).toBeFalsy();
                    pool.query('SELECT id, ST_AsText(trajectory) trajectory from trajectories WHERE id=1', function (err, result) {
                        expect(result[0].id).toBe(1);
                        var p = points.map(function(e) {return e[1] + ' ' + e[2];}).join(',');
                        expect(result[0].trajectory).toBe('LINESTRING('+p+')');
                        done();
                    });
                });
            });
        });

    });
});
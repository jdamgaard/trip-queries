/**
 * Created by jacob on 6/12/15.
 */
'use strict';

var mysql = require('mysql'),
    nconf = require('nconf');

nconf.argv()
    .env()
    .file({ file: './config.json' });

// setup db, with connection restart
var pool;
var connection;

function handleDisconnectConnection() {
    //recreate pool
    connection = mysql.createConnection({
        host     : nconf.get('db:host'),
        user     : nconf.get('db:user'),
        password : nconf.get('db:pass'),
        database : nconf.get('db:db')
    });

    // test connection, if down wait before retry, to avoid hot loop
    connection.connect(function(err) {
        if (err) {
            if (err.code === 'ECONNREFUSED') {
                console.log('DB is down, waiting for retry:');
                setTimeout(handleDisconnectConnection, 2000);
            } else {
                console.log('error when connecting to db:', err);
                setTimeout(handleDisconnectConnection, 2000);
            }
        }
    });

    //bind error handler
    connection.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            setTimeout(handleDisconnectConnection, 2000);
        } else {
            //if not connection error
            throw err;
        }
    });
}

function handleDisconnectPool() {
    //recreate pool
    pool = mysql.createPool({
        connectionLimit: nconf.get('db:connectionLimit'),
        host: nconf.get('db:host'),
        user: nconf.get('db:user'),
        password: nconf.get('db:pass'),
        database: nconf.get('db:db')
    });

    // test connection, if down wait before retry, to avoid hot loop
    pool.getConnection(function(err) {
        if(err) {
            if (err.code === 'ECONNREFUSED') {
                console.log('DB is down, waiting for retry:');
                setTimeout(handleDisconnectPool, 2000);
            } else {
                console.log('error when connecting to db:', err);
                setTimeout(handleDisconnectPool, 2000);
            }
        }
    });

    //bind error handler
    pool.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            setTimeout(handleDisconnectPool, 2000);
        } else {
            //if not connection error
            throw err;
        }
    });
}

handleDisconnectConnection();
handleDisconnectPool();


function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function validData(data) {
    if (data.id === undefined || !isNumber(data.id)) {
        return false;
    }

    if (data.lat === undefined || !isNumber(data.lat)) {
        return false;
    }

    if (data.lon === undefined || !isNumber(data.lon)) {
        return false;
    }

    if (data.ts === undefined || !isNumber(data.ts)) {
        return false;
    }

    return true;
}

function persistEvent(id, ts, lat, lon, cb) {
    pool.query('INSERT INTO rawPoints VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE lat=?, lon=?', [id, ts, lat, lon, lat, lon], cb);
}

//Isolation level should be set to serializable for initial idea to work!
//but mysql seems to ignore isolation level for this connection
//workaround is to use a queue, and serialize work here from client!
var concurrentTripUpdateQueue = [];
var tripUpdateWorkerActive = false;

function updateConcurrentTripsWorker() {
    if (concurrentTripUpdateQueue.length === 0) {
        tripUpdateWorkerActive = false;
        return;
    }
    //pick first element in queue
    var next = concurrentTripUpdateQueue[0];

    connection.query('INSERT INTO concurrentTrips SELECT ?, IFNULL((SELECT trips FROM concurrentTrips where ts <= ? ORDER BY ts DESC LIMIT 1), 0)', [next[0], next[0]], function(err) {
        //ignore duplicate entry, as update is first made in next query
        if (err && err.code !== 'ER_DUP_ENTRY') {
            concurrentTripUpdateQueue.shift();
            updateConcurrentTripsWorker();
            return next[2](err);
        }
        connection.query('UPDATE concurrentTrips SET trips = trips +'+next[1]+' WHERE ts >='+ next[0], function(err) {
            concurrentTripUpdateQueue.shift();
            updateConcurrentTripsWorker();
            next[2](err);
        });
    });
}

function updateConcurrentTrips(ts, change, cb) {
    concurrentTripUpdateQueue.push([ts,change,cb]);
    if (tripUpdateWorkerActive === false) {
        tripUpdateWorkerActive = true;
        updateConcurrentTripsWorker();
    }
}

function getConcurrentTrips(ts, cb) {
    pool.query('SELECT trips FROM concurrentTrips WHERE ts <= '+ts+' ORDER BY ts DESC LIMIT 1', function(err, result) {
        if (result !== undefined && result[0] !== undefined) {
            result = result[0].trips;
        } else {
            result = 0;
        }
        cb(err, result);
    });
}


function beginTrip(id, ts, lat, lon, cb) {
    pool.query('INSERT INTO trips (id, startLat, startLon) VALUES (?,?,?)', [id, lat, lon], function(err) {
        if (err) {
            return cb(err);
        }
        updateConcurrentTrips(ts, 1, cb);
    });
}

function endTrip(id, ts, lat, lon, fare, cb) {
    pool.query('UPDATE trips SET endLat = ?, endLon=?, fare=? WHERE id = ?', [lat, lon, fare, id], function(err) {
        if (err) {
            return cb(err);
        }
        updateConcurrentTrips(ts, -1, cb);
    });
}

function updateFare(id, fare, cb) {
    pool.query('UPDATE trips SET fare=? WHERE id = ?', [fare, id], function(err) {
        if (err) {
            return cb(err);
        }
        return cb();
    });
}

function getNextId(cb) {
    pool.query('SELECT MAX(id) nextId FROM trips',  function(err, result) {
        if (err) {
            return cb(err);
        }
        if (result === undefined || result.length === 0) {
            cb('nextId query failed');
        } else {
            cb(err, result[0].nextId);
        }
    });
}

function getTripFare(id, cb) {
    pool.query('SELECT fare FROM trips WHERE id = ?', id, function(err, result) {
        if (err) {
            return cb(err);
        }
        if (result === undefined || result.length === 0) {
            cb(new Error('Specified trip not found!'));
        } else if (result[0].fare === null) {
            cb(new Error('Specified trip not ended!'));
        } else {
            cb(null, result[0].fare);
        }
    });
}

function getTripFareFromGeoRect(lat1, lon1, lat2, lon2, type, cb) {

    var beginConstraint = mysql.format('(startLat >= ? AND startLat <= ? AND startLon >= ? AND startLon <= ?)', [lat1, lat2, lon1, lon2]);
    var endConstraint = mysql.format('(endLat >= ? AND endLat <= ? AND endLon >= ? AND endLon <= ?)', [lat1, lat2, lon1, lon2]);
    var constraint;
    switch (type) {
        case 'begin':
            constraint = beginConstraint;
            break;
        case 'end':
            constraint = endConstraint;
            break;
        default:
            constraint = beginConstraint + ' OR ' + endConstraint;
    }
    //console.log(constraint);
    pool.query('SELECT count(*) trips, sum(fare) fare FROM trips WHERE fare IS NOT NULL AND ' + constraint, function(err, result) {
        if (err) {
            return cb(err);
        }
        if (result === undefined || result.length === 0) {
            cb(new Error('Error with query! Should not happen!'));
        } else {
            cb(null, result[0]);
        }
    });
}

function saveTrajectory(id, points, cb) {
    var p = points.map(function (p) {return '' + p[1] + ' ' +p[2];}).join(',');
    pool.query('INSERT INTO trajectories VALUES (?, ST_LINESTRINGFROMTEXT(\'LINESTRING('+p+')\'))', [id], function(err) {
        if (err) {
            //console.log ('error with query: \n' + 'INSERT INTO trajectories VALUES (?, ST_LINESTRINGFROMTEXT(\'LINESTRING('+p+')\'))');
            console.log ('error with query: ' + err.message);
        }
        cb(err);
    });
}

function updateTrajectory(id, points, cb) {
    var p = points.map(function (p) {return '' + p[1] + ' ' +p[2];}).join(',');
    pool.query('UPDATE trajectories SET trajectory = ST_LINESTRINGFROMTEXT(\'LINESTRING('+p+')\') WHERE id=?', [id], function(err) {
        if (err) {
            console.log ('error with query: ' + err.message);
        }
        cb(err);
    });
}

function getTripsIntersectingWithRect(lat1, lon1, lat2, lon2, cb) {
    var mbr = '('+lat1+' '+lon1+','+lat1+' '+lon2+','+lat2+' '+lon2+','+lat2+' '+lon1+')';
    pool.query('SELECT count(*) trips FROM trajectories WHERE ST_Intersects(GeomFromText(\'POLYGON('+mbr+')\'),trajectory)', function(err, result) {
        if (err) {
            return cb(err);
        }
        cb(null, result[0].trips);
    });
}

function loadTrip(id, cb) {
    pool.query('SELECT * FROM rawPoints WHERE id = ? ORDER BY ts', [id], function (err, result) {
        cb(err, result);
    });
}

module.exports.validData = validData;
module.exports.persistEvent = persistEvent;

module.exports.getNextId = getNextId;
module.exports.beginTrip = beginTrip;
module.exports.endTrip = endTrip;
module.exports.updateFare = updateFare;
module.exports.getConcurrentTrips = getConcurrentTrips;
module.exports.getTripFare = getTripFare;
module.exports.getTripFareFromGeoRect = getTripFareFromGeoRect;
module.exports.saveTrajectory = saveTrajectory;
module.exports.updateTrajectory = updateTrajectory;
module.exports.getTripsIntersectingWithRect = getTripsIntersectingWithRect;
module.exports.loadTrip = loadTrip;

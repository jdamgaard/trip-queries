'use strict';

var handleData = require('./handleData');

describe('Data validation:', function () {
    it('should allow valid point', function () {
        expect(handleData.validData({id: 1, ts: 1000, lat:56.004, lon: 10.5})).toBe(true);
        expect(handleData.validData({id: 1132156, ts: 1009550, lat:5.004, lon: 1.5})).toBe(true);
    });

    it('should not allow point without position', function () {
        expect(handleData.validData({id: 1, ts: 1000, lat:56.004})).toBe(false);
        expect(handleData.validData({id: 1, ts: 1000, lon: 10.5})).toBe(false);
        expect(handleData.validData({id: 1, ts: 1000, lat:56.004, lon: 'a'})).toBe(false);
        expect(handleData.validData({id: 1, ts: 1000, lat:56.004, lon: null})).toBe(false);
    });

    it('should not allow point without timestamp', function () {
        expect(handleData.validData({id: 1, ts: null, lat:56.004, lon: 10.5})).toBe(false);
        expect(handleData.validData({id: 1, ts: 'ten past 12', lat:56.004, lon: 10.5})).toBe(false);
        expect(handleData.validData({id: 1, ts: '12am', lat:56.004, lon: 10.5})).toBe(false);
        expect(handleData.validData({id: 1, lat:56.004, lon: 10.5})).toBe(false);
    });


});
/**
 * Created by jacob on 6/12/15.
 */
'use strict';

var express = require('express'),
    bodyParser = require('body-parser'),
    datahandler = require('./handleData'),
    nconf = require('nconf'),
    Trip = require('./trip');

nconf.argv()
    .env()
    .file({ file: './config.json' });

//create serverApp
var app = express();
app.use(bodyParser.json());

var stats = {processed: 0, pings: 0, pingTime: 0};

var activeTrips = {};
var pingQueue = {};
var endQueue = {};

var queueWorkers = 0;
var persistQueue = [];
var endTripQueue = [];

/**
 * update trip table, add end location and fare
 * also save trajectory to db, and remove trip from active list, to free working memory
 */
function endTrip(id, ts, lat, lon, cb) {
    //save fare to db
    datahandler.endTrip(id, ts, lat, lon, activeTrips[id].getFare(), function(err) {
        if (err) {
            return cb(new Error('No error expected for endtrip! ' + err.message));
        }

        //save trajectory to db
        datahandler.saveTrajectory(id, activeTrips[id].points, function(err) {
            if (err) {
                return cb(err);
            }

        });

        //remove from active map
        delete activeTrips[id];
        cb();
    });
}

/**
 * Handle ping events received while busy loading or creating new trip
 */
function replayQueue(id) {
    var e;
    // add events received while creating/loading trip
    if (pingQueue[id] !== undefined) {
        // new ping events queued could also be added to db, so ignore duplicates!
        while (pingQueue[id].length > 0) {
            e = pingQueue[id].shift();
            activeTrips[id].addPointOrSkip(e[0], e[1], e[2]);
        }
        delete pingQueue[id];
    }

    // if end trip event was received when loading the trip
    if (endQueue[id] !== undefined) {
        e = endQueue[id];
        try {
            activeTrips[id].end(e[0], e[1], e[2]);
        } catch (err) {
            // Error if point is invalid!
            // Errors include: 'duplicate timestamp', 'end ts before ts for last point', and 'already ended'.
            // Not fatal errors, fare will be calculated from last valid point, which should be close enough.
            // Response sent when event was queued, so cannot notify client at this point.
//          console.log('Cannot notify client of error during recovery: '+err.message);
        }
        endTrip(id, e[0], e[1], e[2], function (err) {
            if (err) {
                if (err.code === 'ECONNREFUSED') {
                    // if connection error, retry later
                    endTripQueue.push([id, e[0], e[1], e[2]]);
                } else {
                    console.log(err.message);
                    //delete activeTrips[id];
                }
            }
        });
        delete endQueue[id];
    }
}

/**
 * Load trip from db, if not exist create new
 * afterwards replay pings received while this is done
 */
function loadAndReplayTrip(id) {
    activeTrips[id] = 'await';
    datahandler.loadTrip(id, function(err, result) {
        if (err) {
            console.log('setting false: ' + err);
            activeTrips[id] = false;
            return;
        }
        if (result.length === 0) {
            //not in db, trip begin event might be delayed!
            //event is already queued, and is replayed when begin event is received.
            return;
        }
        activeTrips[id] = new Trip(id, result[0].ts, result[0].lat, result[0].lon);

        // add points from db to the trip
        // new ping events queued could also be added to db, so ignore duplicates!
        for(var i = 1; i < result.length; i++) {
            activeTrips[id].addPointOrSkip(result[i].ts, result[i].lat, result[i].lon);
        }
        replayQueue(id);
    });
}

/**
 * Saves a point to DB, in case recovery is needed
 */
function persistPoint(id,ts,lat,lon, cb) {
    datahandler.persistEvent(id,ts,lat,lon, function(err) {
        if (err) {
            if (err.code === 'ECONNREFUSED') {
                persistQueue.push([id, ts, lat, lon]);
                return cb(new Error('DB is down'));
            } else {
                console.log(err.message);
                return cb(err);
            }
        }
        cb();
        //if db has been down, and queue is not empty, start worker
        if (persistQueue.length > 0 && queueWorkers < 50) {
            queueWorkers++;
            handleQueue();
        }
    });
}

function handleQueue () {
    var pos = persistQueue.shift();
    if (pos !== undefined) {
        persistPoint(pos[0], pos[1], pos[2], pos[3], function (err) {
            if (err) {
                console.log(err);
            } else {
                handleQueue();
            }
        });
    } else { //pos === undefined
        var elm = endTripQueue.shift();
        if (elm !== undefined) {
            endTrip(elm[0],elm[1], elm[2], elm[3], function () {
                //wait 5 seconds before retry
                setTimeout(function() {
                    endQueue.push(elm);
                    handleQueue();
                }, 5000);
            });

        } else {
            //queues are empty
            queueWorkers--;
            return;
        }
    }
}



// =============  express routes

//TODO auth middleware

/**
 * This middleware gets executed for every request to the app
 * Test that POST requests contains valid data
 */
app.use(function (req, res, next) {
    // if POST request, validate data
    if (req.method === 'POST') {
        stats.processed++;
        // same validation can be used for all POST routes in this app
        if (!datahandler.validData(req.body)) {
            return res.status(400).send('Invalid request');
        }
    }

    next();
});


app.post('/begin', function (req, res) {
    var id = parseInt(req.body.id);
    var ts = parseInt(req.body.ts);
    var lat = parseFloat(req.body.lat);
    var lon = parseFloat(req.body.lon);

    //return status from beginTrip
    activeTrips[id] = 'await';

    //return status from beginTrip
    datahandler.beginTrip(id, ts, lat, lon, function(err) {
        if (err) {
            activeTrips[id] = false;
            console.log(err.message);
            res.status(400).send(err.message);
        } else {
            activeTrips[id] = new Trip(id, ts, lat, lon);
            res.status(200).send();
            replayQueue(id);
        }
    });

    persistPoint(id, ts, lat, lon, function(err) {
        if (err) {
            return console.log(err.message);
        }
    });
});

app.post('/ping', function (req, res) {
    var start = Date.now();

    var id = parseInt(req.body.id);
    var ts = parseInt(req.body.ts);
    var lat = parseFloat(req.body.lat);
    var lon = parseFloat(req.body.lon);
    //add point to trip, load trip if it is not loaded, await if trip is being loaded.
    if (activeTrips[id] === false) {
        console.log('invalid trip: ' + id + ' - ' + ts);
        return res.status(404).send('Invalid trip!');
    } else if (activeTrips[id] === 'await') {
        if (pingQueue[id] === undefined) {
            pingQueue[id] = [];
        }
        pingQueue[id].push([ts, lat, lon]);
    } else if (activeTrips[id] === undefined) {
        if (pingQueue[id] === undefined) {
            pingQueue[id] = [];
        }
        pingQueue[id].push([ts, lat, lon]);
        loadAndReplayTrip(id);
    } else {
        //loaded
        try {
            activeTrips[id].addPoint(ts, lat, lon);
            // If a point for trip is delayed to after end, then update fare and trajectory in db.
            if (activeTrips[id].endTime !== null) {
                datahandler.updateFare(id, activeTrips[id].getFare(), function(err) {
                    if (err) {
                        console.log('no error expected for update fare! ' + err);
                    }
                });
                datahandler.updateTrajectory(id, activeTrips[id].points, function(err) {
                    if (err) {
                        console.log (err);
                    }
                });
            }
        } catch (e) {
            //console.log(activeTrips[id]);
            return res.status(400).send('Error with point: ' + e.message);
        }
    }

    persistPoint(id, ts, lat, lon, function(err) {
        stats.pings++;
        stats.pingTime += Date.now() - start;
        if (err) {
            return res.status(500).send(err.message);
        }
        res.status(202).send();
    });
});

app.post('/end', function (req, res) {
    var id = parseInt(req.body.id);
    var ts = parseInt(req.body.ts);
    var lat = parseFloat(req.body.lat);
    var lon = parseFloat(req.body.lon);


    if (activeTrips[id] === false) {
        return res.status(404).send('Invalid trip!');
    } else if (activeTrips[id] === 'await') {
        endQueue[id] = [ts, lat, lon];
    } else if (activeTrips[id] === undefined) {
        endQueue[id] = [ts, lat, lon];
        loadAndReplayTrip(id);
    } else {
        try {
            activeTrips[id].end(ts, lat, lon);
        } catch (e) {
            res.status(400).send('Error with point: ' + e.message);
            return;
        }
        endTrip(id, ts, lat, lon, function (err) {
            if (err) {
                if (err.code === 'ECONNREFUSED') {
                    // if connection error, retry later
                    endTripQueue.push([id, ts, lat, lon]);
                } else {
                    console.log(err.message);
                    //delete activeTrips[id];
                }
            }
        });
    }

    persistPoint(id, ts, lat, lon, function(err) {
        if (err) {
            return res.status(500).send(err.message);
        }
        res.status(202).send();
    });
});

// fetch id to use from DB, could be delegated to other app to ensure unique ids
var nextTripId = 0;
app.get('/getNextId', function (req, res) {
    //consider sending error, if server load is too high
    if (nextTripId === 0) {
        nextTripId = -1;
        datahandler.getNextId(function (err, result) {
            if (err) {
                nextTripId = 0;
                return res.status(500).send(err.message);
            }
            nextTripId = result + 1;
            res.status(200).send('{"id":'+ nextTripId +'}');
            nextTripId++;
        });
    } else if (nextTripId < 0) {
        res.status(500).send('Server not ready jet');
    } else {
        res.status(200).send('{"id":'+ nextTripId +'}');
        nextTripId++;
    }
});

function formatFare(id, fare) {
    //todo format fare more nicely
    return '{"tripId": '+id+', "fare": '+fare+'}';
}

// /fare?id=<id>
app.get('/fare', function (req, res) {
    if (req.query.id !== undefined) {
        var id = parseInt(req.query.id);
        if(isNaN(id)) {
            return res.status(400).send('{msg: "invalid \'id\'"}');
        }
        //if active trip, then make local calculation, else fetch from db
        if (activeTrips[id] !== undefined && typeof activeTrips[id] === 'object') {
            try {
                return res.status(200).send(formatFare(id, activeTrips[id].getFare()));
            } catch (err) {
                return res.status(500).send('{"msg":"Error with trip!"}');
            }
        } else {
            datahandler.getTripFare(id, function (err, fare) {
                if (err) {
                    if (err.message === 'Specified trip not found!') {
                        return res.status(404).send('{"msg":"'+err.message+'"}');
                    }
                    return res.status(500).send('{"msg":"'+err.message+'"}');
                }
                return res.status(200).send(formatFare(id, fare));
            });
        }
    } else {
        return res.status(400).send('{"msg": "remember query \'id\'"}');
    }
});

// /tripLocation?rect=<lat>,<lon>,<lat>,<lon>[&type=[all|begin|end]]
// query begin, end or either in a geo-rect, and the sum of fares.
// only queries trips ended so far!
app.get('/tripLocation', function (req, res) {
    var lat1, lat2, lon1, lon2;
    var type = 'all';
    try {
        var rect = req.query.rect.split(',');
        lat1 = parseFloat(rect[0]);
        lon1 = parseFloat(rect[1]);
        lat2 = parseFloat(rect[2]);
        lon2 = parseFloat(rect[3]);
        if(isNaN(lat1) || isNaN(lat2) || isNaN(lon1) || isNaN(lon2)) {
            return res.status(400).send('{"msg":"Invalid syntax for geo-rect"}');
        }
        if(lat2 < lat1 || lon2 < lon1) {
            return res.status(400).send('{"msg":"geo-rect should be «minLat, minLon, maxLat, maxLon»"}');
        }
        if (req.query.type !== undefined) {
            if (req.query.type === 'all' || req.query.type === 'begin' || req.query.type === 'end') {
                type = req.query.type;
            }
        }
    } catch (err) {
        return res.status(400).send(err);
    }

    datahandler.getTripFareFromGeoRect(lat1, lon1, lat2, lon2, type, function (err, result) {
        if (err) {
            return res.status(500).send(err.message);
        }
        return res.status(200).send(JSON.stringify(result));
    });

});

// /tripIntersects?rect=<lat>,<lon>,<lat>,<lon>
// query trips intersecting with geo rect,
// The trips either have a point inside the geo-rect,
// or the line segment connecting 2 on each following points on the trip, intersects with the geo-rect.
// It only queries trips ended so far!
app.get('/tripIntersects', function (req, res) {
    var lat1, lat2, lon1, lon2;
    try {
        var rect = req.query.rect.split(',');
        lat1 = parseFloat(rect[0]);
        lon1 = parseFloat(rect[1]);
        lat2 = parseFloat(rect[2]);
        lon2 = parseFloat(rect[3]);
        if(isNaN(lat1) || isNaN(lat2) || isNaN(lon1) || isNaN(lon2)) {
            return res.status(400).send('{"msg":"Invalid syntax for geo-rect"}');
        }
        if(lat2 < lat1 || lon2 < lon1) {
            return res.status(400).send('{"msg":"geo-rect should be «minLat, minLon, maxLat, maxLon»"}');
        }
    } catch (err) {
        return res.status(400).send(err);
    }

    datahandler.getTripsIntersectingWithRect(lat1, lon1, lat2, lon2, function (err, result) {
        if (err) {
            return res.status(500).send(err);
        }
        return res.status(200).send('{"tripIntersects": '+result+'}');
    });

});

// /concurrentTrips?ts=<ts>
// Given a timestamp, returns the number of concurrent trips at that point in time.
app.get('/concurrentTrips', function (req, res) {

    if (req.query.ts !== undefined) {
        var ts = parseInt(req.query.ts);
        if(isNaN(ts)) {
            return res.status(400).send('{"msg": "invalid \'ts\'"}');
        }

        datahandler.getConcurrentTrips(ts, function (err, result) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send({concurrentTrips: result});
            }
        });
    } else {
        res.status(400).send('{"msg": "remember query \'ts\'"}');
    }
});


//print stats at regular intervals
function logStats() {
    var msg = 'Msg received:' + (stats.processed);

    if (stats.pings > 0) {
        msg += '\tAvg for ping: ' + (stats.pingTime / stats.pings).toFixed(3) + 'ms';
    }

    if (persistQueue.length > 0) {
        msg += '\tQueued: '+persistQueue.length + ' - workers: ' + queueWorkers;
    }
    console.log(msg);
    stats = {processed: 0, pings: 0, pingTime: 0};
}
setInterval(logStats, nconf.get('LOG_FREQ') * 1);

app.listen(8088, function() {
    console.log('%s listening at %s', 'Uber server', 8088);
});

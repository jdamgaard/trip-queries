/**
 * Created by jacob on 6/13/15.
 */
'use strict';

var trip = function(id, ts, lat, lon) {
    this.distance = 0;
    this.startTime = ts;
    this.endTime = null;
    this.points = [[ts,lat,lon]];
    this.priceDetails = {
        'distance_unit': 'km',
        'cost_per_minute': 0.65,
        'service_fees': [],
        'minimum': 15.0,
        'cost_per_distance': 3.75,
        'base': 8.0,
        'cancellation_fee': 10.0,
        'currency_code': 'USD'
    };
};

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

trip.prototype.validPoint = function (ts,lat,lon) {
    if (ts < this.startTime) {
        return false;
    } else if (this.endTime && ts > this.endTime) {
        return false;
    } else {
        return (isNumber(lat) && isNumber(lon));
    }
};

// distance between two latitude,longitude pairs.
// based on ideas from http://www.movable-type.co.uk/scripts/latlong.html
function toRadians(deg) {
    return deg * Math.PI / 180;
}

// Equirectangular projection, returns approximate distance in km
// The difference compared to the haversine is negligible for the short distances used, and this solution is faster.
function projectedDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; // Earth radius in km
    var x = toRadians(lon2-lon1) * Math.cos(toRadians(lat1+lat2)/2);
    var y = toRadians(lat2-lat1);
    return Math.sqrt(x*x + y*y) * R;
}


trip.prototype.addPoint = function (ts,lat,lon) {

    var last = this.points[this.points.length - 1];
    if (!this.validPoint(ts, lat, lon)) {
        throw new Error('Invalid point');
    }
    if (ts === last[0]) {
        throw new Error('duplicate timestamp');
    }

    //if ts < last point, e.g. packet was delayed, insert before, and shift
    if (ts < last[0]) {
        //find index
        var index = this.points.length - 2;
        while (ts <= this.points[index][0]) {
            if (ts === this.points[index][0]) {
                throw new Error('duplicate timestamp');
            }
            index--;
        }
        //insert point after found index
        index++;
        this.points.splice(index, 0, [ts,lat,lon]);
        //update distance, replace previous with new going through the new point
        var prev = this.points[index-1];
        var next = this.points[index+1];
        this.distance -= projectedDistance(prev[1], prev[2], next[1], next[2]);
        this.distance += projectedDistance(prev[1], prev[2], lat, lon);
        this.distance += projectedDistance(lat, lon, next[1], next[2]);

    } else {
        this.distance += projectedDistance(last[1], last[2], lat, lon);
        this.points.push([ts,lat,lon]);
    }
};

trip.prototype.addPointOrSkip = function(ts,lat,lon) {
    try {
        this.addPoint(ts, lat, lon);
    } catch (err) {
        if (err.message !== 'duplicate timestamp') {
            console.log('Error adding point' + err);
        }
    }
};

trip.prototype.end = function (ts,lat,lon) {
    if (this.endTime !== null) {
        throw new Error('already ended!');
    }
    if (this.points[this.points.length - 1] >= ts) {
        throw new Error('cannot end trip earlier than points on trip!');
    }
    this.addPoint(ts,lat,lon);
    this.endTime = ts;
};

trip.prototype.getFare = function () {
    var fare = this.priceDetails.base;
    var last = this.points[this.points.length - 1];

    var time = last[0] - this.startTime;
    fare += time/(60*1000) * this.priceDetails.cost_per_minute;

    fare += this.distance * this.priceDetails.cost_per_distance;

    return Math.max(this.priceDetails.minimum, fare);
};

module.exports = trip;
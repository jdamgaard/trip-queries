/**
 * Created by jacob on 6/13/15.
 */
'use strict';

var Trip = require('./trip');

describe('trip valid point:', function() {
    it('should accept valid point', function (done) {
        var myTrip = new Trip(1, 100, 56.10, 10.10);

        expect(myTrip.validPoint(119,56.1,10.1)).toBe(true);
        done();
    });

    it('should not have ping events before start time', function (done) {
        var myTrip = new Trip(1, 100, 56.10, 10.10);

        expect(myTrip.validPoint(10,56.1,10.1)).toBe(false);
        done();
    });

    it('should not have ping events after end time', function (done) {
        var myTrip = new Trip(1, 100, 56.10, 10.10);

        myTrip.end(120,56,10);
        expect(myTrip.validPoint(125,56.1,10.1)).toBe(false);
        done();
    });

    it('should accept valid but delayed point', function (done) {
        var myTrip = new Trip(1, 100, 56.10, 10.10);

        myTrip.end(120,56,10);
        expect(myTrip.validPoint(119,56.1,10.1)).toBe(true);
        done();
    });

    it('should throw error on invalid data point', function(done) {
        var myTrip = new Trip(1, 1434228200000, 56.10, 10.10);

        expect(function() {
            myTrip.addPoint(1434228200000, 56.10, null);
        }).toThrow(new Error('Invalid point'));

        done();
    });

});


describe('trip:', function() {

    it('should use minimum cost at start', function(done) {
        var myTrip = new Trip(1, 1434228200000, 56.10, 10.10);

        myTrip.priceDetails.minimum = 10;
        expect(myTrip.getFare()).toBe(10);

        done();
    });

    it('should include cost pr time', function(done) {
        var myTrip = new Trip(1, 1000000, 56.10, 10.10);
        myTrip.priceDetails.minimum = 5;

        myTrip.priceDetails.base = 0;
        myTrip.priceDetails.cost_per_minute = 1;

        //1 min
        myTrip.addPoint(1060000, 56.10, 10.10);
        expect(myTrip.getFare()).toBe(5, 'min cost should be used');

        //6 min
        myTrip.addPoint(1360000, 56.10, 10.10);
        expect(myTrip.getFare()).toBe(6);

        //10 min
        myTrip.addPoint(1600000, 56.10, 10.10);
        expect(myTrip.getFare()).toBe(10);

        done();
    });

    it('should include cost pr distance', function(done) {
        var myTrip = new Trip(1, 1000000, 56.10, 10.10);
        myTrip.priceDetails.minimum = 5;

        myTrip.priceDetails.base = 0;
        myTrip.priceDetails.cost_per_minute = 0;
        myTrip.priceDetails.cost_per_distance = 1;
        myTrip.distance = 20;

        expect(myTrip.getFare()).toBe(20.00, 'expect 20km á 1$');

        done();
    });

    it('should calculate distance', function(done) {
        var myTrip = new Trip(1, 1000000, 56.10, 10.10);

        // distance between (56.10,10.10) and (56.11,10.11) is ~ 1.27317 km
        myTrip.addPoint(1000001, 56.11, 10.11);
        expect(myTrip.distance.toFixed(5)).toBe('1.27317');

        // distance between (56.11,10.11) and (56.13,10.105) is ~ 2.24539 km
        myTrip.addPoint(1000002, 56.13, 10.105);
        expect(myTrip.distance.toFixed(5)).toBe('3.51856');

        done();
    });

    it('should calculate short distance', function(done) {
        var myTrip = new Trip(1, 1000000, 56.10, 10.10);

        // distance at that resolution ~ 0.11m
        myTrip.addPoint(1000001, 56.100001, 10.1000000);
        expect(myTrip.distance.toFixed(5)).toBe('0.00011');

        done();
    });

    it('should include base cost', function(done) {
        var myTrip = new Trip(1, 1000000, 56.10, 10.10);
        myTrip.priceDetails.minimum = 10;

        myTrip.priceDetails.base = 5;
        myTrip.priceDetails.cost_per_minute = 1;
        myTrip.priceDetails.cost_per_distance = 2;

        //1 min
        myTrip.addPoint(1060000, 56.10, 10.10);
        expect(myTrip.getFare()).toBe(10, 'min cost should be used');

        //6 min
        myTrip.addPoint(1360000, 56.10, 10.10);
        expect(myTrip.getFare()).toBe(11);

        //10 min, 2km
        myTrip.addPoint(1600000, 56.10, 10.10);
        myTrip.distance = 2;
        expect(myTrip.getFare()).toBe(5+10+4, 'costs: base 5, time 10, distance 4');

        done();
    });


    it('should keep points sorted by time stamp', function(done) {
        var myTrip = new Trip(1, 10, 56.10, 10.10);
        myTrip.addPoint(25, 56.101, 10.101);
        myTrip.addPoint(15, 56.10, 10.10);

        expect(myTrip.points[myTrip.points.length-1]).toEqual([25, 56.101, 10.101]);

        done();
    });

    it('should update distance when delayed point is added', function(done) {
        var myTrip = new Trip(1, 1000000, 56.10, 10.10);

        // distance between (56.10,10.10) and (56.11,10.11) is ~ 1.27317 km
        // distance between (56.11,10.11) and (56.13,10.105) is ~ 2.24539 km
        // distance between (56.10,10.10) and (56.13,10.105) is ~ 3.35022 km

        myTrip.addPoint(1001500, 56.13, 10.105);
        expect(myTrip.distance.toFixed(5)).toBe('3.35022', 'distance is wrong!');

        myTrip.addPoint(1000500, 56.11, 10.11);
        expect(myTrip.distance.toFixed(5)).toBe('3.51856', 'earlier point was added');

        done();
    });


    it('should throw error on duplicate data point', function(done) {
        var myTrip = new Trip(1, 1434228200000, 56.10, 10.10);

        expect(function() {
                myTrip.addPoint(1434228200000, 56.10, 10.10);
            }).toThrow(new Error('duplicate timestamp'));

        done();
    });



});